import gitlab
import subprocess
import os


def get_artifacts(private_token, project_id, pipeline_ref):
    print(private_token)
    print(project_id)
    print(pipeline_ref)
    # private token or personal token authentication
    gl = gitlab.Gitlab('http://gitlab.com', private_token)
    archery_project = gl.projects.get(project_id)
    master_pipeline = None
    for pipeline in archery_project.pipelines.list():
        if pipeline.ref == pipeline_ref and pipeline.status == "success":
            master_pipeline = pipeline
            break
    if master_pipeline == None:
        print("there isn't a pipeline with the provided parameters")
        return
    pipeline_job = pipeline.jobs.list()[0]
    job = archery_project.jobs.get(pipeline_job.id, lazy=True)
    zipfn = "artifacts.zip"
    with open(zipfn, "wb") as f:
        job.artifacts(streamed=True, action=f.write)
    subprocess.run(["unzip", "-bo", zipfn])
    os.unlink(zipfn)

if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--token", help="A gitlab private token")
    parser.add_argument("-p", "--projectId", help="Project Id", type=int)
    parser.add_argument("-r", "--ref", help="Pipeline ref for where extract the artifacts")
    args = parser.parse_args()
    get_artifacts(args.token, args.projectId, args.ref)

